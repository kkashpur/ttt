<?php
/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Tic Tac Toe';
});
/**
 * @see \App\Http\Controllers\GameController
 */
$router->group(['prefix' => 'api/v1/games', 'as' => 'games', 'middleware' => ['session']], function($router) {
    /** @var \Laravel\Lumen\Routing\Router $router */
    $router->get('/', ['uses' => 'GameController@index', 'as' => 'index']);
    $router->post('/', ['uses' => 'GameController@create', 'as' => 'create']);
    $router->get('/{id}', ['uses' => 'GameController@show', 'as' => 'show']);
    $router->put('/{id}', ['uses' => 'GameController@update', 'as' => 'update']);
    $router->delete('/{id}', ['uses' => 'GameController@delete', 'as' => 'delete']);
});
