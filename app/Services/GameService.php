<?php

namespace App\Services;

use App\Models\Game;
use Faker\Factory;

class GameService
{
    const DEFAULT_BOARD = '---------';
    const X = 'X';
    const O = 'O';
    const UNSET = '-';
    const MAX_MOVES = 9;
    const WINNER_MOVES = 4;

    private $wins = [
        '111000000',
        '000111000',
        '000000111',
        '100100100',
        '010010010',
        '001001001',
        '001010100',
        '100010001',
    ];

    /**
     * @param string $board
     * @return Game
     */
    public function create(string $board=self::DEFAULT_BOARD)
    {
        return new Game(Factory::create()->uuid, $board, Game::STATUS_RUNNING);
    }

    /**
     * @param array $games
     * @return array|Game[]
     */
    public function all(array $games)
    {
        $result = [];
        foreach ($games as $game) {
            array_push($result, $this->get($game));
        }
        return $result;
    }

    /**
     * @param array $game
     * @return Game
     */
    public function get(array $game)
    {
        return new Game($game['id'], $game['board'], $game['status']);
    }

    /**
     * @param Game $game
     * @param string $board
     * @return Game
     */
    public function move(Game $game, string $board): Game
    {
        $moves = substr_count($board, self::X) * 2 - 1;
        if ($moves === self::MAX_MOVES) {
            $game->setStatus(Game::STATUS_DRAW);
        } else {
            $firstUnset = strpos($board, self::UNSET);
            if ($firstUnset !== false) {
                $board = substr_replace($board, self::O, $firstUnset, 1);
            }
        }
        if ($moves > self::WINNER_MOVES) {
            if ($this->checkWinner($board, self::X, self::O)) {
                $game->setStatus(Game::STATUS_X_WON);
            } else if ($this->checkWinner($board, self::O, self::X)) {
                $game->setStatus(Game::STATUS_O_WON);
            }
        }

        return $game->setBoard($board);
    }

    /**
     * @param string $board
     * @param string $winner
     * @param string $looser
     * @return bool
     */
    private function checkWinner(string $board, string $winner, string $looser): bool
    {
        $binBoard = str_replace($winner, '1',
                        str_replace($looser, '0',
                            str_replace(self::UNSET, '0', $board)));
        foreach($this->wins as $win) {
            if (in_array($binBoard & $win, $this->wins)) {
                return true;
            }
        }
        return false;
    }
}
