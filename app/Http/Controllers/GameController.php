<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Services\GameService;
use App\Transformers\GameTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\ResponseFactory;
use Laravel\Lumen\Routing\Controller as BaseController;
use Spatie\Fractalistic\ArraySerializer as ArraySerializerAlias;

/**
 * Class GameController
 * @package App\Http\Controllers
 * @covers GameControllerTest
 */
class GameController extends BaseController
{
    /**
     * @var GameService
     */
    private GameService $gameService;

    /**
     * GameController constructor.
     * @param GameService $gameService
     */
    public function __construct(GameService $gameService)
    {
        $this->gameService = $gameService;
    }

    /**
     * @param Request $request
     * @param array $errors
     * @return JsonResponse|Response|ResponseFactory|mixed
     */
    public function buildFailedValidationResponse(Request $request, array $errors)
    {
        $reason = '';
        foreach($errors as $error) {
            $reason .= implode(' ', $error);
        }
        return response(['reason' => $reason], 400);
    }

    /**
     * @return string
     */
    public function index()
    {
        $games = [];

        return fractal()
            ->collection($this->gameService->all($games))
            ->transformWith(new GameTransformer())
            ->serializeWith(new ArraySerializerAlias())
            ->toJson();
    }

    /**
     * @param Request $request
     * @return Response|ResponseFactory
     * @throws ValidationException
     */
    public function create(Request $request)
    {
        $this->validateRequest($request);
        $game = $this->gameService->create($request->board);
        $result = fractal($game, new GameTransformer())->serializeWith(new ArraySerializerAlias());
        Cache::put($game->getId(), $result->toArray());
        return response(['location' => $game->getId()], 201);
    }

    /**
     * @param string $id
     * @return Response|ResponseFactory
     */
    public function show(string $id)
    {
        $result = fractal($this->getGame($id), new GameTransformer());
        return response($result->serializeWith(new ArraySerializerAlias())->toJson());
    }

    /**
     * @param Request $request
     * @param string $id
     * @return Response|ResponseFactory
     * @throws ValidationException
     */
    public function update(Request $request, string $id)
    {
        $this->validateRequest($request);
        $game = $this->gameService->move($this->getGame($id), $request->board);
        $result = fractal($game, new GameTransformer())->serializeWith(new ArraySerializerAlias());
        Cache::put($game->getId(), $result->toArray());
        return response($result->toJson());
    }

    /**
     * @param string $id
     * @return Response|ResponseFactory
     */
    public function delete(string $id)
    {
        if (!Cache::has($id)) {
            abort(404);
        }
        Cache::forget($id);
        return response('');
    }

    /**
     * Aborts if no games were found
     * @param string $id
     * @return Game
     */
    private function getGame(string $id)
    {
        $game = Cache::get($id);
        if (is_null($game)) {
            abort(404);
        }
        return $this->gameService->get($game);
    }

    /**
     * @param Request $request
     * @return array
     * @throws ValidationException
     */
    private function validateRequest(Request $request)
    {
        return $this->validate($request, ['board' => 'required|regex:/([XO\-]+)/|size:9']);
    }
}
