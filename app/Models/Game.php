<?php

namespace App\Models;

class Game
{
    const STATUS_RUNNING = 'RUNNING';
    const STATUS_X_WON = 'X_WON';
    const STATUS_O_WON = 'O_WON';
    const STATUS_DRAW = 'DRAW';
    /**
     * @var string
     */
    private string $id;
    /**
     * @var string
     */
    private string $board;
    /**
     * @var string
     */
    private string $status;
    /**
     * Game constructor.
     * @param string $id
     * @param string $board
     * @param string $status
     */
    public function __construct(string $id, string $board, string $status)
    {
        $this->id = $id;
        $this->board = $board;
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBoard(): string
    {
        return $this->board;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param string $board
     * @return $this
     */
    public function setBoard(string $board): self
    {
        $this->board = $board;
        return $this;
    }
}
