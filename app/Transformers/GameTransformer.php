<?php

namespace App\Transformers;

use App\Models\Game;
use League\Fractal\TransformerAbstract;

class GameTransformer extends TransformerAbstract
{
    public function transform(Game $game)
    {
        return [
            'id' => $game->getId(),
            'board' => $game->getBoard(),
            'status' => $game->getStatus()
        ];
    }
}
