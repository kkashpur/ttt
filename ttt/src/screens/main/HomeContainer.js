import React, { Component } from 'react';
import {Link} from "react-router-dom";

export default class HomeContainer extends Component {
    render() {
        return (
            <div>
                <div>Tic Tac Toe</div>
                <Link to="/games">games</Link>
            </div>
        );
    }
}
