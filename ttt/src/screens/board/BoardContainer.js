import React, { Component } from 'react';
import axios from "../../shared/request";
import "./Board.css"
import {USER_MOVE, SERVER_MOVE, STATUS_RUNNING, STATUS_SERVER_WIN, STATUS_USER_WIN} from "./constants"
import {API_UPDATE, API_GET_DETAILS} from "./constants/api"

export default class BoardContainer extends Component {
    constructor(props) {
        super(props);
        const { params } = props.match
        this.state = {
            isFinished: false,
            game: '',
            id: params.id
        };
    }

    componentDidMount() {
        axios.get(API_GET_DETAILS.replace(':id', this.state.id))
            .then(response => this.setState({ game: response.data }))
    }
    notify = () => {
        alert(`The game is over.${this.state.game.status === STATUS_SERVER_WIN
            ? 'I won' : this.state.game.status === STATUS_USER_WIN
                ? 'You won' : ''}`);
    }

    onClick = (event) => {
        if (!this.state.isFinished) {
            const id = parseInt(event.currentTarget.id);
            let {board} = this.state.game;
            board = (board.substring(0, id) + USER_MOVE + board.substring(id + 1));

            axios.put(API_UPDATE.replace(':id', this.state.id), {'board': board})
                .then(response => {
                    const isFinished = response.data.status !== STATUS_RUNNING;
                    this.setState({ game: response.data, isFinished: isFinished });
                    if (isFinished) {
                        this.notify();
                    }
                });
        } else {
            this.notify();
        }
    };

    render() {
        return (
            <ul className="game"> {
                this.state.game && this.state.game.board.split('').map((item, index) =>
                    <li className={`${item === SERVER_MOVE ? 'nought' : item === USER_MOVE ? 'cross' : 'blank'}`}
                        onClick={this.onClick}
                        id={index}
                        key={index}
                    > </li>)
            } </ul>
        );
    }
}
