import {API_MAIN} from "../../../shared/request"

export const API_GET_DETAILS = `${API_MAIN}games/:id`;
export const API_UPDATE = `${API_MAIN}games/:id`;
