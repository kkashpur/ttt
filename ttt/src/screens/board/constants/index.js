export const USER_MOVE = 'X';
export const SERVER_MOVE = 'O';

export const STATUS_USER_WIN = 'X_WON';
export const STATUS_SERVER_WIN = 'O_WON';
export const STATUS_RUNNING = 'RUNNING';
export const STATUS_DRAW = 'DRAW';
