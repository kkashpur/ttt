import React, { Component } from 'react';
import List from "./components/List";
import axios, {URL_BOARD} from "../../shared/request";
import {API_POST, API_GET_LIST} from "./constants/api"
import {DEFAULT_BOARD} from "./constants";

export default class GamesContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            games: []
        };
    }

    componentDidMount() {
        axios.get(API_GET_LIST)
            .then(response => this.setState({ games: response.data }));
    }

    onClick = () => {
        axios.post(API_POST, { 'board': DEFAULT_BOARD })
            .then(response =>
                this.props.history.push(URL_BOARD.replace(':id', response.data.location)));
    }

    render() {
        return (
            <div>
                <List items={this.state.games}/>
                <button onClick={this.onClick}>new</button>
            </div>
        );
    }
}
