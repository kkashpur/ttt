import {API_MAIN} from "../../../shared/request"

export const API_GET_LIST = `${API_MAIN}games`;
export const API_POST = `${API_MAIN}games`;
export const API_DELETE = `${API_MAIN}games/:id`;
