import React from 'react';
import {Link} from "react-router-dom";

const List = props => (
    <ul>
        { props.items.map((item, index) =>
            <li key={index}><Link to={`detail/${item.id}`}>{item.id}</Link></li>) }
    </ul>
);

export default List;
