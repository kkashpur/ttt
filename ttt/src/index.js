import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { HashRouter as Router } from 'react-router-dom'
import { Route } from 'react-router'

import {URL_HOME, URL_GAMES, URL_BOARD} from './shared/request'
import HomeContainer from './screens/main/HomeContainer'
import GamesContainer from "./screens/games/GamesContainer";
import BoardContainer from "./screens/board/BoardContainer";

const routing = (
    <Router>
        <div>
            <Route exact path={URL_HOME} component={HomeContainer} />
            <Route path={URL_GAMES} component={GamesContainer} />
            <Route path={URL_BOARD} component={BoardContainer} />
        </div>
    </Router>
)
ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
