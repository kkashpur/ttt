export const URL_HOME = '/';
export const URL_GAMES = '/games';
export const URL_BOARD = '/board/:id';

export const API_MAIN = 'http://localhost/api/v1/';

const axios = require('axios');

export default axios;
