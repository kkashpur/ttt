# Tic Tac Toe

To Run you need to have a docker and docker-compose and npm.
All frontend part is located under ```ttt``` subfolder.
It is possible to run just with ```docker-compose up``` and ```npm start```.
 
##First Run
For a frontend and backend you need to install dependencies
```npm install``` and ```docker-compose up composer```

##Tests
```docker-compose up test```
