<?php
namespace Http\Controllers;

use App\Http\Controllers\GameController;
use App\Models\Game;
use Faker\Factory;
use Illuminate\Support\Facades\Cache;
use TestCase;

/**
 * Class GameControllerTest
 * @package Http\Controllers
 * @see GameController
 */
class GameControllerTest extends TestCase
{
    const ID = "a073fc4d-8aac-3dee-8017-20b34d8e21b0";
    const BOARD = "XXX---OOO";
    const STATUS = Game::STATUS_RUNNING;

    private function defineGame()
    {
        Cache::put(self::ID, ['id' => self::ID, 'board' => self::BOARD, 'status' => self::STATUS]);
    }

    public function testIndex()
    {
        $this->defineGame();
        $this->get(route('games.index'));
        $this->seeJsonContains([]);
        $this->assertResponseStatus(200);
    }

    public function tesEmptyIndex()
    {
        $this->get(route('games.index'));
        $this->assertResponseStatus(200);
    }

    public function testCreat()
    {
        $this->post(route('games.create', ['board' => self::BOARD]));
        $this->seeJsonStructure(['location']);
        $this->assertResponseStatus(201);
    }

    public function testCreatShort()
    {
        $this->post(route('games.create', ['board' => 'X-O']));
        $this->seeJson(['reason' => 'The board must be 9 characters.']);
        $this->assertResponseStatus(400);
    }

    public function testCreatRequired()
    {
        $this->post(route('games.create'));
        $this->seeJson(['reason' => 'The board field is required.']);
        $this->assertResponseStatus(400);
    }

    public function testCreatWrong()
    {
        $this->post(route('games.create', ['board' => 123456789]));
        $this->seeJson(['reason' => 'The board format is invalid.']);
        $this->assertResponseStatus(400);
    }

    public function testShow()
    {
        $this->defineGame();
        $this->get(route('games.show', ['id' => self::ID]));
        $this->seeJsonContains(['id' => self::ID, 'board' => self::BOARD, 'status' => self::STATUS]);
        $this->assertResponseStatus(200);
    }

    public function testShowNonExisting()
    {
        $this->get(route('games.show', ['id' => Factory::create()->uuid]));
        $this->assertResponseStatus(404);
    }

    public function testUpdate()
    {
        $this->defineGame();
        $this->put(route('games.update', ['id' => self::ID, 'board' => 'X--------']));
        $this->seeJson(['status' => 'RUNNING']);
    }

    public function testUpdateXwon()
    {
        $this->defineGame();
        $this->put(route('games.update', ['id' => self::ID, 'board' => 'XXXOOOOOO']));
        $this->seeJson(['status' => Game::STATUS_X_WON]);
    }

    public function testUpdateOwon()
    {
        $this->defineGame();
        $this->put(route('games.update', ['id' => self::ID, 'board' => 'OOOX-X-X-']));
        $this->seeJson(['status' => Game::STATUS_O_WON]);
    }

    public function testUpdateDraw()
    {
        $this->defineGame();
        $this->put(route('games.update', ['id' => self::ID, 'board' => 'XXOOOXXOX']));
        $this->seeJson(['status' => Game::STATUS_DRAW]);
    }

    public function testUpdateNonExisting()
    {
        $this->put(route('games.update', ['id' => Factory::create()->uuid, 'board' => self::BOARD]));
        $this->assertResponseStatus(404);
    }

    public function testUpdateWrong()
    {
        $this->put(route('games.update', ['id' => self::ID, 'board' => 123456789]));
        $this->seeJson(['reason' => 'The board format is invalid.']);
        $this->assertResponseStatus(400);
    }

    public function testUpdateShort()
    {
        $this->put(route('games.update', ['id' => self::ID, 'board' => 'X-O']));
        $this->seeJson(['reason' => 'The board must be 9 characters.']);
        $this->assertResponseStatus(400);
    }

    public function testDelete()
    {
        $this->defineGame();
        $this->delete(route('games.delete', ['id' =>self::ID]));
        $this->assertResponseOk();
    }

    public function testDeleteNonExisting()
    {
        $this->delete(route('games.delete', ['id' => Factory::create()->uuid]));
        $this->assertResponseStatus(404);
    }
}
