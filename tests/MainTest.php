<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class MainTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRootExample()
    {
        $this->get('/');

        $this->assertEquals(
            'Tic Tac Toe', $this->response->getContent()
        );
    }
}
